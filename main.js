"use strict"

let admin;
let username;

username = "Vladyslav";
admin = username;
console.log(username);

// Перетворити число на кількість секунд і вивести в консоль

let days = Math.floor(Math.random() * 10) + 1;
let seconds = days * 24 * 60 * 60;
console.log("There are " + seconds + " seconds " + "in " + days + " days");
console.log(days, typeof(days));

// Запитати у користувача якесь значення і вивести його в консоль

const USER_HEIGHT = + prompt("How tall are you? (in cm)");
console.log(USER_HEIGHT, typeof(USER_HEIGHT));